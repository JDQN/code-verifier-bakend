import express, { Request, Response } from 'express';
import { UserController } from '../controller/UsersController';
import { LogInfo } from '../utils/logger';


//Router from express 
let usersRouter = express.Router();


//http://localhost:8000/api/hello?name=JuanD/

usersRouter.route('/')
//GET -> http://localhost:8000/api/users

.get(async (req: Request, res: Response) => {

   //Obtain a query param (ID)
   let id: any = req?.params?.id;
   LogInfo(`Query Param: ${id}`);

   //Aqui planteamos una instancia d econtroller para usar un method
   const controller: UserController = new UserController();

   //Aui obtenemos la respuesta del metodo del controller
   const response: any = await controller.getUsers();
   return res.send(response);

});


//Aqui hacemos un export de hello router
export default usersRouter;