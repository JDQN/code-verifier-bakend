import { Get, Query, Route, Tags } from 'tsoa';
import { IUserController } from './interfaces';
import { LogSuccess, LogError } from '../utils/logger';


//ORM USERS
import { getAllUsers } from '../domain/orm/User.orm';
import { BasicResponse } from './types';


@Route("/api/users")
@Tags("Users")


export class UserController implements IUserController{


   /**
   *Endpoint to retreive the users in the colletion "User" of DB 
   */

   @Get("/")
   public async getUsers(@Query()id?: string): Promise<any> {

      let response: any = '';
      
      if(id){
         LogSuccess(`[/api/users] Get User By ID: ${id}`);
         response = await getUserByID(id);
      }else{
         LogSuccess('[/api/users] Getting All User Request');
         response = await getAllUsers();
      }
      return response;
   }

}




