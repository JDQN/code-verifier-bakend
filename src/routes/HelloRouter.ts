import { BasicResponse } from '@/controller/types';
import express, { Request, Response } from 'express';
import { HelloController } from '../controller/HelloController';
import { LogInfo } from '../utils/logger';


//Router from express 
let helloRouter = express.Router();


//http://localhost:8000/api/hello?name=JuanD/

helloRouter.route('/')
//GET -> http://localhost:8000/api/hello?name=JuanD/

.get(async (req: Request, res: Response) => {

   //Aqui obtenemos a Query Paramas
   let name: any = req?.query?.name;
   LogInfo(`Query Param: ${name}`);

   //Aqui planteamos una instancia d econtroller para usar un method
   const controller: HelloController = new HelloController();

   //Aui obtenemos la respuesta del metodo del controller
   const response: BasicResponse = await controller.getMessage(name);

   //Aqui obtenemos la respuesta
   return res.send(response);

});


//Aqui hacemos un export de hello router
export default helloRouter;