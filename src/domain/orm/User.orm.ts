/*
* La carpeta orm me sirve para guardar los modelos de datos de la base de datos
*/
import { userEntity } from '../entities/user.entity';
import { LogSuccess, LogError } from 'src/utils/logger';


//CRUD A qui vamos a realizar todas las peticiones
/**
* Method to abtain all User from collection "User" in MongoDB
*/


export const getAllUsers = async () => {
   try {
      let usersModel = userEntity();

      //Search all users
      return  await usersModel.find({ isDelete: false });
   } catch (error) {
      LogError(`[ORM ERROR]: Getting All Users: ${error}`);
   }
}

//TODO: 
// falta hacer un get User By ID
export const getUsersByID = async (id: string) : Promise<any | undefined> => {
   try {
      let usersModel = userEntity();
      return await usersModel.findById(id);

   } catch (error) {
      LogError(`[ORM ERROR]: Getting Users By ID: ${error}`);
   }
}
// falta hacer un get User By Email
// falta hacer un Delete User By ID
// falta hacer un Create New User
// falta hacer un Update User By ID