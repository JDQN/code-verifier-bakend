import { Get, Query, Route, Tags } from 'tsoa';
import { BasicResponse } from './types';
import { IHellOController } from './interfaces';
import { LogSuccess } from '../utils/logger';



@Route('/api/hello')
@Tags("HelloController")



export class HelloController implements IHellOController {

/**
*Endpoint to retreive a Message "Hello {name}" in JSON 
*@param name {string | undefined} Name of the message user to be greeted
*@returns {BasicResponse} Promise of BasicResponse
*/

@Get('/')
   public async getMessage(name?: string): Promise<BasicResponse> {
      LogSuccess('[/api/hello] Get Request');

      return {
         message: `Hello ${name || 'World'}` 
      }
   }   
}