import mongoose from 'mongoose';

export const userEntity = () => {

   let userSchema = new mongoose.Schema(
      {
         name: String,
         email: String,
         age: Number
      }
   )

   //return mongoose.model("User", userSchema);
   return mongoose.models.Users || mongoose.model('Users', userSchema);
}

//Aqui estamos creando un modelo de mongo atraves de TS